//
//  SATDataModelTests.swift
//  NYCSchoolsTests
//

import XCTest
@testable import NYCSchools

class SATDataModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSATDataModelHappyPathTests() throws {
        let dataModels: [SATDataModel] = try DataHelperUtils.loadDataModels(jsonFileName: "satdatamodels")
        XCTAssertEqual(dataModels.count, 5)
        
        let firstModel = dataModels.first

        XCTAssertEqual(firstModel?.dbn, "01M292")
        XCTAssertEqual(firstModel?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(firstModel?.numOfSatTestTakers, "29")
        XCTAssertEqual(firstModel?.satCriticalReadingAvgScore, "355")
        XCTAssertEqual(firstModel?.satMathAvgScore, "404")
        XCTAssertEqual(firstModel?.satWritingAvgScore, "363")

    }
    
    func testMissingDataVariations() throws {
        let dataModels: [SATDataModel] = try DataHelperUtils.loadDataModels(jsonFileName: "satdatamodels")
        XCTAssertEqual(dataModels.count, 5)
        
        var dataModel = dataModels.filter { $0.schoolName == "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL"}.first
        XCTAssertEqual(dataModel?.numOfSatTestTakers, "91")
        XCTAssertEqual(dataModel?.satCriticalReadingAvgScore, "383")
        XCTAssertEqual(dataModel?.satMathAvgScore, "423")
        XCTAssertNil(dataModel?.satWritingAvgScore)
        
        dataModel = dataModels.filter { $0.schoolName == "EAST SIDE COMMUNITY SCHOOL"}.first
        XCTAssertEqual(dataModel?.numOfSatTestTakers, "70")
        XCTAssertEqual(dataModel?.satCriticalReadingAvgScore, "377")
        XCTAssertEqual(dataModel?.satWritingAvgScore, "370")
        XCTAssertNil(dataModel?.satMathAvgScore)
        
        dataModel = dataModels.filter { $0.schoolName == "FORSYTH SATELLITE ACADEMY"}.first
        XCTAssertEqual(dataModel?.numOfSatTestTakers, "7")
        XCTAssertEqual(dataModel?.satMathAvgScore, "401")
        XCTAssertEqual(dataModel?.satWritingAvgScore, "359")
        XCTAssertNil(dataModel?.satCriticalReadingAvgScore)
        
        dataModel = dataModels.filter { $0.schoolName == "MARTA VALLE HIGH SCHOOL"}.first
        XCTAssertEqual(dataModel?.satCriticalReadingAvgScore, "390")
        XCTAssertEqual(dataModel?.satMathAvgScore, "433")
        XCTAssertEqual(dataModel?.satWritingAvgScore, "384")
        XCTAssertNil(dataModel?.numOfSatTestTakers)
    }
}
