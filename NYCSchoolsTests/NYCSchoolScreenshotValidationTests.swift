//
//  NYCSchoolScreenshotValidationTests.swift
//  NYCSchoolsUITests
//

import XCTest
@testable import NYCSchools
import SnapshotTesting

/// Snapshot test example
class NYCSchoolScreenshotValidationTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHSListViewController() throws {
        // TODO: Setup `HSListViewModel` mocks
        let vc = HSListViewController()
        assertSnapshot(matching: vc, as: .image)
    }
    
    func testListDetailViewController() throws {
        let dataModels: [HSDataModel] = try DataHelperUtils.loadDataModels(jsonFileName: "hsdatamodels")
        XCTAssertEqual(dataModels.count, 10)
        
        let firstModel = dataModels.first
        
        XCTAssertEqual(firstModel?.dbn, "02M260")
        XCTAssertEqual(firstModel?.schoolName, "Clinton School Writers & Artists, M.S. 260")
        
        let vc = HSDetailViewController(model: firstModel, satDataModel: nil)
        assertSnapshot(matching: vc, as: .image)
    }
    //TODO: Add additional screen variations for testing
}
