//
//  DataHelperUtils.swift
//  NYCSchoolsTests
//

import Foundation
@testable import NYCSchools
import CoreLocation

extension CLLocationCoordinate2D: Equatable {
    static public func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}

/// Test helper utility
class DataHelperUtils {
    /// A utility to load json file with and decode it into a model object
    /// - Parameter jsonFileName: Json file name (without extension)
    /// - Returns: An array of model objects of type T
    static func loadDataModels<T: Codable>(jsonFileName: String) throws -> [T] {
        guard
            let path = Bundle(for: self).path(forResource: jsonFileName, ofType: "json")
        else {
            fatalError("Can't find \(jsonFileName).json file")
        }
        
        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        let models = try JSONDecoder().decode([T].self, from: data)
        return models
    }
    
    // Happy path
    static var firstModelAddress: String {
        "10 East 15th Street, Manhattan NY 10003"
    }
    
    static var firstModelContact: String {
        "212-524-4360 / admissions@theclintonschool.net"
    }
    
    static var firstModelAcademicOpportunities: String {
        """
        Free college courses at neighboring universities \
        International Travel, Special Arts Programs, Music, Internships, College Mentoring \
        English Language Learner Programs: English as a New Language
        """
    }

    static var firstModelGettingThereText: String {
        """
        Borough: MANHATTAN, Neighborhood: Chelsea-Union Sq
        Bus: BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, \
        M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9
        Subway: 1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St
        """
    }
    
    static var firstModelLatLong: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: CLLocationDegrees(40.73653), longitude: CLLocationDegrees(-73.9927))
    }
    
    static var secondModelAcademicOpportunities: String {
        """
        CAMBA, Diploma Plus, Medgar Evers College, Coney Island Genera on Gap, Urban Neighborhood Services, \
        Coney Island Coalition Against Violence, I Love My Life Initiative, New York City Police Department
        """
    }
}
