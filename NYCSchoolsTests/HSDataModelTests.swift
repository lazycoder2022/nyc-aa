//
//  HSDataModelTests.swift
//  NYCSchoolsTests
//

import XCTest
@testable import NYCSchools
import MapKit

class HSDataModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHSDataModelHappyPathTests() throws {
        let dataModels: [HSDataModel] = try DataHelperUtils.loadDataModels(jsonFileName: "hsdatamodels")
        XCTAssertEqual(dataModels.count, 10)
        
        let firstModel = dataModels.first
        
        XCTAssertEqual(firstModel?.dbn, "02M260")
        XCTAssertEqual(firstModel?.schoolName, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertEqual(firstModel?.attendanceRate, "0.970000029")
        
        XCTAssertEqual(firstModel?.attendanceRatePercentage, "97")
        
        XCTAssertEqual(firstModel?.academicOpportunities, DataHelperUtils.firstModelAcademicOpportunities)
        
        XCTAssertEqual(firstModel?.gettingThere, DataHelperUtils.firstModelGettingThereText)
                       
        XCTAssertEqual(firstModel?.latlong, DataHelperUtils.firstModelLatLong)
    }
    
    
    func testAcademicOpportunitiesVariations() throws {
        let dataModels: [HSDataModel] = try DataHelperUtils.loadDataModels(jsonFileName: "hsdatamodels")
        XCTAssertEqual(dataModels.count, 10)
        
        let dataModel = dataModels.filter { $0.schoolName == "Liberation Diploma Plus High School" }.first
        XCTAssertNil(dataModel?.academicopportunities1)
        XCTAssertNotNil(dataModel?.academicopportunities2)
        
        XCTAssertEqual(dataModel?.academicOpportunities, DataHelperUtils.secondModelAcademicOpportunities)
    }
    
    func testEmailNilVariation() throws {
        let dataModels: [HSDataModel] = try DataHelperUtils.loadDataModels(jsonFileName: "hsdatamodels")
        XCTAssertEqual(dataModels.count, 10)
        
        let dataModel = dataModels.filter { $0.schoolName == "Women's Academy of Excellence" }.first
        XCTAssertNil(dataModel?.schoolEmail)
        XCTAssertNotNil(dataModel?.phoneNumber)
        
        XCTAssertEqual(dataModel?.contact, dataModel?.phoneNumber)
    }
    
    func testPhoneNilVariation() throws {
        let dataModels: [HSDataModel] = try DataHelperUtils.loadDataModels(jsonFileName: "hsdatamodels")
        XCTAssertEqual(dataModels.count, 10)
        
        let dataModel = dataModels.filter { $0.schoolName == "Brooklyn School for Music & Theatre" }.first
        XCTAssertNil(dataModel?.phoneNumber)
        XCTAssertNotNil(dataModel?.schoolEmail)
        
        XCTAssertEqual(dataModel?.contact, dataModel?.schoolEmail)
    }
    // TODO: Add variations for other test cases i.e. Getting there etc.
}
