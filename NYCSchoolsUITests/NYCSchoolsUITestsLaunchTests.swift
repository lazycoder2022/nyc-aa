//
//  NYCSchoolsUITestsLaunchTests.swift
//  NYCSchoolsUITests
//

import XCTest

class NYCSchoolsUITestsLaunchTests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }
    
    /// Test to navigate through all the app pages
    func testLaunch() throws {
        let app = XCUIApplication()
        app.launch()
        app.tables["tableView-HSListViewController"].cells.element(boundBy: 0).tap()
        XCTAssertEqual(app/*@START_MENU_TOKEN@*/.staticTexts["titleLabel-HSSummaryView"]/*[[".otherElements[\"HSSummaryView\"]",".staticTexts[\"Clinton School Writers & Artists, M.S. 260\"]",".staticTexts[\"titleLabel-HSSummaryView\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.label, "Clinton School Writers & Artists, M.S. 260")
        
        XCTAssertEqual(app/*@START_MENU_TOKEN@*/.scrollViews.otherElements.staticTexts["Overview"]/*[[".otherElements[\"HSDetailView\"]",".otherElements[\"CarouselView\"].scrollViews.otherElements",".otherElements.matching(identifier: \"CarouselPageView\").staticTexts[\"Overview\"]",".staticTexts[\"Overview\"]",".scrollViews.otherElements"],[[[-1,4,2],[-1,1,2],[-1,0,1]],[[-1,4,2],[-1,1,2]],[[-1,3],[-1,2]]],[0,0]]@END_MENU_TOKEN@*/.label, "Overview")
        app/*@START_MENU_TOKEN@*/.scrollViews.otherElements.staticTexts["Overview"]/*[[".otherElements[\"HSDetailView\"]",".otherElements[\"CarouselView\"].scrollViews.otherElements",".otherElements.matching(identifier: \"CarouselPageView\").staticTexts[\"Overview\"]",".staticTexts[\"Overview\"]",".scrollViews.otherElements"],[[[-1,4,2],[-1,1,2],[-1,0,1]],[[-1,4,2],[-1,1,2]],[[-1,3],[-1,2]]],[0,0]]@END_MENU_TOKEN@*/.swipeLeft()
                
        XCTAssertEqual(app.scrollViews.otherElements.staticTexts["Getting there"].label, "Getting there")
        app.scrollViews.otherElements.staticTexts["Getting there"].swipeLeft()
        
        XCTAssertEqual(app.scrollViews.otherElements.staticTexts["Academics"].label, "Academics")
        app/*@START_MENU_TOKEN@*/.scrollViews.otherElements.staticTexts["Academics"]/*[[".otherElements[\"HSDetailView\"]",".otherElements[\"CarouselView\"].scrollViews.otherElements",".otherElements.matching(identifier: \"CarouselPageView\").staticTexts[\"Academics\"]",".staticTexts[\"Academics\"]",".scrollViews.otherElements"],[[[-1,4,2],[-1,1,2],[-1,0,1]],[[-1,4,2],[-1,1,2]],[[-1,3],[-1,2]]],[0,0]]@END_MENU_TOKEN@*/.swipeRight()
        
        app.navigationBars["NYCSchools.HSDetailView"].buttons["NYC Schools"].tap()
    }
    
    /// Test to verify workings of search controller (Happy path)
    func testSearchController() throws {
        let app = XCUIApplication()
        app.launch()
        
        let nycSchoolsNavigationBar = app.navigationBars["NYC Schools"]
        let searchSchoolsSearchField = nycSchoolsNavigationBar.searchFields["Search schools"]
        searchSchoolsSearchField.tap()
        searchSchoolsSearchField.typeText("Clinton")
        app.tables["tableView-HSListViewController"]/*@START_MENU_TOKEN@*/.staticTexts["718-543-1000 / dwc@dwchs.net"]/*[[".cells.matching(identifier: \"HSListViewCell\").staticTexts[\"718-543-1000 \/ dwc@dwchs.net\"]",".staticTexts[\"718-543-1000 \/ dwc@dwchs.net\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        XCTAssertEqual(app/*@START_MENU_TOKEN@*/.staticTexts["titleLabel-HSSummaryView"]/*[[".otherElements[\"HSSummaryView\"]",".staticTexts[\"Clinton School Writers & Artists, M.S. 260\"]",".staticTexts[\"titleLabel-HSSummaryView\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.label, "DeWitt Clinton High School")
        
        app.navigationBars["NYCSchools.HSDetailView"].buttons["NYC Schools"].tap()
        
        searchSchoolsSearchField.tap()
        nycSchoolsNavigationBar.buttons["Cancel"].tap()
    }
    // TODO: Add other use cases around navigation and screens
}
