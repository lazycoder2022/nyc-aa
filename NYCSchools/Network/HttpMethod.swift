//
//  HttpMethod.swift
//  NYCSchools
//

import Foundation

/// HTTP method for a HTTP request
/// - Seealso: RFC 7231  https://datatracker.ietf.org/doc/html/rfc7231.html#section-4
enum HttpMethod: String {
    case get = "GET"
    case head = "HEAD"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case connect = "CONNECT"
    case options = "OPTIONS"
    case trace = "TRACE"
}
