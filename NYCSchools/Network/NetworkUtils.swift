//
//  NetworkUtils.swift
//  NYCSchools
//

import Foundation

struct NetworkUtils {
    // Constant keys
    static let contentTypeKey = "Content-Type"
    static let acceptKey = "Accept"
    static let localeKey = "locale"
    static let transactionIdKey = "transaction_id"
    static let userAgentKey = "User-Agent"
    static let appTokenKey = "X-App-Token"
    // Constant values
    static let jsonContentType = "application/json"
    static let appToken = "IQf3Th99RtwqnRPe3v84q78XR"
    
    /// Returns common headers required for making a url request
    /// - Returns: A dictionary containing http headers
    static func commonHeaders() async throws -> [String: String] {
        let locale = Locale(identifier: "EN_US")
        let headers: [String: String] = [
            contentTypeKey: jsonContentType,
            acceptKey: jsonContentType,
            localeKey: locale.identifier,
            transactionIdKey: NetworkUtils.uuidString,
            userAgentKey: NetworkUtils.userAgent,
            appTokenKey: appToken
        ]
        return headers
    }
    
    /// Returns a lower cased uuid string
    /// Ex: "f321e1f3-d36c-435a-93fc-0c345a3e6e5f"
    static var uuidString: String {
        return UUID().uuidString.lowercased()
    }
    
    /// A User-Agent string for header
    static var userAgent: String {
        return "NYCSChools/V1.0/iOS"
    }
}
