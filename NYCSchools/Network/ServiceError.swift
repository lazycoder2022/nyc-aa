//
//  ServiceErrors.swift
//  NYCSchools
//

import Foundation

/// Enum representing service errors
enum ServiceError: Error {
    /// Invalid url
    case invalidUrl
}
