//
//  NYCHSDataApiService.swift
//  NYCSchools
//

import Foundation

/// High school data api service implementation
final class NYCHSDataApiService {
    let session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
    
    /// Fetch school data
    /// - Returns: Decoded school data in the form of `HSDataModel`
    func fetchNYCHSData() async throws -> [HSDataModel] {
        return try await decodedData(for: session).decoded
    }
}

extension NYCHSDataApiService {
    /// Base url
    static var baseUrl: String {
        return "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    }
}

// MARK: - HttpService protocol conformance
extension NYCHSDataApiService: HttpService{
    /// Response type
    typealias Response = [HSDataModel]
    
    /// Http method (get) for this request
    var httpMethod: HttpMethod {
        return .get
    }
    
    /// URL for the request
    /// - Returns: A URL object if valid, otherwise this methid throws an error
    func url() async throws -> URL {
        guard
            let url = URL(string: NYCHSDataApiService.baseUrl)
        else {
            throw ServiceError.invalidUrl
        }
        return url
    }
    
    /// Headers
    /// - Returns: A dictionary containing headers
    func headers() async throws -> [String: String] {
        let headers = try await NetworkUtils.commonHeaders()
        return headers
    }
    
    /// Body data
    /// - Returns: Optional body. In this case, its not needed
    func body() async throws -> Data? {
        return nil
    }
}
