//
//  HttpService.swift
//  NYCSchools
//

import Foundation

/// HttpService protocol to fetch / post data. This service uses
protocol HttpService {
    /// Response type associated with the request
    associatedtype Response
    
    /// Http method associated with the request.
    /// SEE `HttpMethod` for available types
    var httpMethod: HttpMethod { get }
    
    /// URL for the request
    /// - Returns: Returns a URL or throws an error
    func url() async throws -> URL
    
    /// Http headers (including auth headers).
    /// - Returns: A dictionary containing http headers. Throws an error if an error is encountered
    func headers() async throws -> [String: String]
    
    /// Body of the request
    /// - Returns: Http body if available. Throws an error if needed
    func body() async throws -> Data?
    
    /// Decode `data` to the supplied `Response` type
    /// - Parameters:
    ///   - response: `Response` type
    ///   - data: Response data will be decoded
    /// - Returns: `Response`
    func decode(response: URLResponse, data: Data) async throws -> Response
}

extension HttpService where Response: Decodable {
    /// Decode `Data` response (Default implementation) where Response type is `Decodable`
    /// - Parameters:
    ///   - response: `Response` type
    ///   - data: Response data will be decoded
    /// - Returns: `Response`
    func decode(response: URLResponse, data: Data) async throws -> Response {
        let decoder = JSONDecoder()
        return try decoder.decode(Response.self, from: data)
    }
}

extension HttpService where Response == Data {
    
    /// Decode `Data` response (Default implementation) where Response type is `Data`. In this implementaiton, it simply returns supplied Data back
    /// - Parameters:
    ///   - response: `Response` type
    ///   - data: Response data
    /// - Returns: `Response`
    func decode(response: URLResponse, data: Data) async throws -> Response {
        return data
    }
}


// MARK: Request / Response handling
extension HttpService {
    /// Convenience type alias to represent Decoded Response data
    typealias DecodedResponse = (decoded: Response, urlResponse: URLResponse)
    /// Convenience type alias to represent Decoded Response data
    typealias DataResponse = (data: Data, urlResponse: URLResponse)
    
    /// Constructs and returns a url request
    /// - Returns: URLRequest
    func urlRequest() async throws -> URLRequest {
        async let headers = headers()
        async let url = url()
        async let body = body()
        
        var request = URLRequest(url: try await url)
        request.allHTTPHeaderFields = try await headers
        request.httpMethod = httpMethod.rawValue
        request.httpBody = try await body
        return request
    }
    
    /// Execute a `URLRequest` and process response
    /// - Parameter session: URLSession
    /// - Returns: A tuple `DecodedResponse`
    func decodedData(for session: URLSession) async throws -> DecodedResponse {
        return try await internalDecodedData(for: session)
    }
    
    // MARK: - Helper functions
    
    /// Execute a `URLRequest` and process response
    /// - Parameter session: URLSession
    /// - Returns: A tuple `DecodedResponse`
    private func internalDecodedData(for session: URLSession) async throws -> DecodedResponse {
        let request = try await urlRequest()
        let response: DataResponse
        response = try await session.data(for: request)
        // TODO: Handle various http status codes
        let decoded = try await decode(response: response.urlResponse, data: response.data)
        return (decoded, response.urlResponse)
    }
}
