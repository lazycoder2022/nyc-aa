//
//  NYCHSDataApiService.swift
//  NYCSchools
//

import Foundation

/// Service implementation to fetch SAT data from a remote data provider
final class NYCSATDataApiService {
    let session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
    
    /// Fetch SAT data from remote service
    /// - Returns: SAT data decoded to `SATDataModel`
    func fetchNYCSATData() async throws -> [SATDataModel] {
        return try await decodedData(for: session).decoded
    }
}

extension NYCSATDataApiService {
    /// Base url
    static var baseUrl: String {
        return "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
}

extension NYCSATDataApiService: HttpService{
    /// Reponse type `SATDataModel`
    typealias Response = [SATDataModel]
    
    /// Http method (get)
    var httpMethod: HttpMethod {
        return .get
    }
    
    /// URL
    /// - Returns: A `URL` if valid string is supplied
    func url() async throws -> URL {
        guard
            let url = URL(string: NYCSATDataApiService.baseUrl)
        else {
            throw ServiceError.invalidUrl
        }
        return url
    }
    
    /// Http headers
    /// - Returns: A dictionry of http headers required to make a call
    func headers() async throws -> [String: String] {
        let headers = try await NetworkUtils.commonHeaders()
        return headers
    }
    
    func body() async throws -> Data? {
        return nil
    }
}
