//
//  HSListViewCell.swift
//  NYCSchools
//

import Foundation
import UIKit

/// A cell to display high school details on a list screen
class HSListViewCell: UITableViewCell {
    /// High school name
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "nameLabel-HSListViewCell"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .natural
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    /// Address label
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "addressLabel-HSListViewCell"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .natural
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    /// Contact label
    private lazy var contactLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "phoneLabel-HSListViewCell"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .natural
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    /// Website information
    private lazy var websiteLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "emailLabel-HSListViewCell"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .natural
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        return label
    }()
    
    /// Separator view to separate individual cells
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.accessibilityIdentifier = "separatorView-HSListViewCell"
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 0.3).isActive = true
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    /// A view encapsulating various labels
    private lazy var hsSummaryView: UIView = {
        let view = UIView()
        view.accessibilityIdentifier = "hsSummaryView-HSListViewCell"
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    /// A stack view encapsulating all the cell contents
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.accessibilityIdentifier = "contentStackView-HSListViewCell"
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 8
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }
    
    func setupViews() {
        accessibilityIdentifier = "HSListViewCell"
        hsSummaryView.addSubview(nameLabel)
        hsSummaryView.addSubview(addressLabel)
        hsSummaryView.addSubview(contactLabel)
        hsSummaryView.addSubview(websiteLabel)
        contentStackView.addArrangedSubview(hsSummaryView)
        contentView.addSubview(contentStackView)
        contentView.addSubview(separatorView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
                contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                contentStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                contentStackView.bottomAnchor.constraint(equalTo: separatorView.topAnchor, constant: -16),
                
                hsSummaryView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
                
                nameLabel.topAnchor.constraint(equalTo: hsSummaryView.topAnchor, constant: 16),
                nameLabel.leadingAnchor.constraint(equalTo: hsSummaryView.leadingAnchor, constant: 16),
                nameLabel.trailingAnchor.constraint(equalTo: hsSummaryView.trailingAnchor, constant: -8),
                nameLabel.bottomAnchor.constraint(equalTo: addressLabel.topAnchor, constant: -8),

                addressLabel.leadingAnchor.constraint(equalTo: hsSummaryView.leadingAnchor, constant: 16),
                addressLabel.trailingAnchor.constraint(equalTo: hsSummaryView.trailingAnchor, constant: -8),
                addressLabel.bottomAnchor.constraint(equalTo: contactLabel.topAnchor, constant: -8),

                contactLabel.leadingAnchor.constraint(equalTo: hsSummaryView.leadingAnchor, constant: 16),
                contactLabel.trailingAnchor.constraint(equalTo: hsSummaryView.trailingAnchor, constant: -8),
                contactLabel.bottomAnchor.constraint(equalTo: websiteLabel.topAnchor, constant: -8),
                
                websiteLabel.leadingAnchor.constraint(equalTo: hsSummaryView.leadingAnchor, constant: 16),
                websiteLabel.trailingAnchor.constraint(equalTo: hsSummaryView.trailingAnchor, constant: -8),
                websiteLabel.bottomAnchor.constraint(equalTo: hsSummaryView.bottomAnchor, constant: 8),
                
                separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
                separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
                separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8)
            ]
        )
    }
    
    func configure(with dataModel: HSDataModel) {
        nameLabel.text = dataModel.schoolName
        addressLabel.text = dataModel.address
        contactLabel.text = dataModel.contact
        websiteLabel.text = dataModel.website
    }
}
