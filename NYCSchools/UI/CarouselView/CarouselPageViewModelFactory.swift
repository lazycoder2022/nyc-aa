//
//  CarouselPageViewModelFactory.swift
//  NYCSchools
//

import Foundation
import UIKit
import MapKit

/// A factory class to create various "pages" for the detail view
struct CarouselPageViewModelFactory {
    /// Create various page view models. For now, this function returns Overview, Location and Academics page
    /// - Parameters:
    ///   - hsDataModel: Data model with school data
    ///   - satDataModel: SAT data model with SAT score information
    /// - Returns: An array that can be used to display set of pages for high school details
    static func createPageViewModels(hsDataModel: HSDataModel, satDataModel: SATDataModel?) -> [CarouselPageViewModel] {
        return [overviewPageViewModel(hsDataModel: hsDataModel),
                locationPageViewModel(hsDataModel: hsDataModel),
                academicsPageViewModel(hsDataModel: hsDataModel, satDataModel: satDataModel)
        ]
    }
}

// MARK: - Overview page
extension CarouselPageViewModelFactory {
    /// Create overview page view model
    /// - Parameter hsDataModel: Data model with school data
    /// - Returns: An instance of `CarouselPageViewModel` with data for school overview
    static func overviewPageViewModel(hsDataModel: HSDataModel) -> CarouselPageViewModel {
        let vm = CarouselPageViewModel(
            title: "Overview",  // TODO: localization
            subtitle: hsDataModel.academicOpportunities,
            genericView: overviewView(hsDataModel: hsDataModel)
        )
        return vm
    }
    
    /// Returns a view containing Overview infromation
    /// - Parameter hsDataModel: Data model 
    /// - Returns: Returns a view containing Overview infromation
    static func overviewView(hsDataModel: HSDataModel) -> UIView {
        let tv = UITextView()
        tv.text = hsDataModel.overviewParagraph
        tv.isEditable = false
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.font = .systemFont(ofSize: 16, weight: .light)
        tv.preservesSuperviewLayoutMargins = true
        return tv
    }
}

// MARK: - Location
extension CarouselPageViewModelFactory {
    /// Create location page view model
    /// - Parameter hsDataModel: Data model with school data
    /// - Returns: An instance of `CarouselPageViewModel` with data for school location
    static func locationPageViewModel(hsDataModel: HSDataModel) -> CarouselPageViewModel {
        let vm = CarouselPageViewModel(
            title: "Getting there",
            subtitle: hsDataModel.gettingThere,
            genericView: locationView(hsDataModel: hsDataModel)
        )
        return vm
    }
    
    /// Returns a location view (i.e. Map view) with school location selected
    /// - Parameter hsDataModel: Data model with school data
    /// - Returns: Returns a location view
    static func locationView(hsDataModel: HSDataModel) -> UIView {
        let mv = MKMapView()
        mv.translatesAutoresizingMaskIntoConstraints = false
        if let latlong = hsDataModel.latlong {
            let annotation = MKPointAnnotation()
            annotation.coordinate = latlong
            annotation.title = hsDataModel.schoolName
            let coordinateRegion = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 400, longitudinalMeters: 400)
            mv.setRegion(coordinateRegion, animated: true)
            mv.addAnnotation(annotation)
            }
        return mv
    }
}

// MARK: - Academics
extension CarouselPageViewModelFactory {
    /// Create academics page view model
    /// - Parameter hsDataModel: Data model with school data
    /// - Parameter satDataModel: SAT data model with sat data for the school
    /// - Returns: An instance of `CarouselPageViewModel` with academic data (including SAT)
    static func academicsPageViewModel(hsDataModel: HSDataModel, satDataModel: SATDataModel?) -> CarouselPageViewModel {
        let vm = CarouselPageViewModel(
            title: "Academics",
            subtitle: hsDataModel.extraActivities,
            genericView: academicsView(hsDataModel: hsDataModel, satDataModel: satDataModel)
        )
        return vm
    }
    
    /// Returns a view containing academic information
    /// - Parameter hsDataModel: Data model with school data
    /// - Parameter satDataModel: SAT data model with sat data for the school
    /// - Returns: A view with various academic elements laid out
    static func academicsView(hsDataModel: HSDataModel, satDataModel: SATDataModel?) -> UIView {
        let av = HSAcademicsView(hsDataModel: hsDataModel, satModel: satDataModel)
        av.translatesAutoresizingMaskIntoConstraints = false
        return av
    }
}
