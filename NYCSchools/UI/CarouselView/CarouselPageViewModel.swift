//
//  CarouselPageViewModel.swift
//  NYCSchools
//

import UIKit

/// A view model represnting `CarouselPageView`
struct CarouselPageViewModel {
    /// Title of the page
    public let title: String?
    /// Subtitle of the page
    public let subtitle: String?
    /// Generic view that is supplied by the view model
    public let genericView: UIView
    
    /// Initialize CarouselPageViewModel
    /// - Parameters:
    ///   - title: Title
    ///   - subtitle: Subtitle
    ///   - genericView: View provided by the view model
    public init(
        title: String? = nil,
        subtitle: String? = nil,
        genericView: UIView = UIView()
    ) {
        self.title = title
        self.subtitle = subtitle
        self.genericView = genericView
    }
    
    func createView() -> UIView {
        let view = CarouselPageView()
        view.viewModel = self
        return view
    }
}
