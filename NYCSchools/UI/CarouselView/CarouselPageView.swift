//
//  CarouselPageView.swift
//  NYCSchools
//

import UIKit

/// A view representing a Carousel page.
class CarouselPageView: UIView {
    /// Title of the page
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "titleLabel-CarouselPageView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .natural
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 20)
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.text = "Overview"
        return label
    }()
    
    /// Subtitle
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "subtitleLabel-CarouselPageView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .natural
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.text = "Subtitle"
        return label
    }()
    
    /// Content view. This is typically supplied by the view model
    private lazy var contentView: UIView = {
        let view = UIView()
        view.accessibilityIdentifier = "contentView-CarouselPageView"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    /// View model belonging to this view
    var viewModel = CarouselPageViewModel() {
        didSet {
            updateView(viewModel: viewModel)
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupConstraints()
    }
    
    // MARK: - ViewProtocol implementation
    func setupView() {
        accessibilityIdentifier = "CarouselPageView"
        setupBorders()
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(contentView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),

            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8.0),
            subtitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            subtitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),

            contentView.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 16),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
        ])
    }
    public func updateView(viewModel: CarouselPageViewModel) {
        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
        contentView  = viewModel.genericView
        addSubview(contentView)
        
        setupConstraints()
        setupBorders()
        
    }
    
    func setupBorders() {
        layer.cornerRadius = 16
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0.3
    }
}


