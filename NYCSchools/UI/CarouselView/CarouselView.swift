//
//  CarouselView.swift
//  NYCSchools
//

import UIKit

/// A horizontal carousel view with page control
class CarouselView: UIView, ViewProtocol {
    /// Scroll view encapsulating the views
    private let scrollView = UIScrollView()
    /// Container stackview
    private let stackView = UIStackView()
    /// Page control for indicating which page is being displayed
    private let pageControlView = UIPageControl()
    /// View model associated with `CarouselView`
    var viewModel = CarouselViewModel() {
        didSet {
            updateView(viewModel: viewModel)
        }
    }
    /// Keep track of current page index (0-based index)
    var currentPageIndex: Int {
        return Int(round(scrollView.contentOffset.x / (bounds.width != 0 ? bounds.width : 1)))
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupConstraints()
    }
    
    func updateView(viewModel: CarouselViewModel) {
        // Setup stack view contents and its constraints.
        stackView.removeAllArrangedSubviews()
        for page in viewModel.pages {
            stackView.addArrangedSubview(page.createView())
        }
        setupStackViewContentConstraints()
        
        setupPageControlView(viewModel: viewModel)
        
        // Scroll to the page at startingIndex.
        scrollToPage(atIndex: 0, animated: true)
    }
    
    /// Scroll to a specific page
    /// - Parameters:
    ///   - index: Index to scroll to
    ///   - animated: Animated the transition
    private func scrollToPage(atIndex index: Int, animated: Bool) {
        DispatchQueue.main.async {
            var frame: CGRect = self.scrollView.frame
            frame.origin = CGPoint(x: frame.size.width * CGFloat(index), y: 0)
            self.scrollView.scrollRectToVisible(frame, animated: animated)
        }
    }
    
    /// Handles page controller tap
    /// - Parameter sender: Sender that initiated this action
    @objc func pageControlTapped(_ sender: Any) {
        guard let pageControl = sender as? UIPageControl else { return }
        let selectedPage = pageControl.currentPage 
        scrollToPage(atIndex: selectedPage, animated: true)
    }
}
// MARK: - ViewProtocol handlers
extension CarouselView {
    func setupView() {
        accessibilityIdentifier = "CarouselView"
        
        backgroundColor = .clear
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self

        scrollView.isUserInteractionEnabled = true
        scrollView.isScrollEnabled = true
        scrollView.alwaysBounceHorizontal = false
        scrollView.alwaysBounceVertical = false
        addSubview(scrollView)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 0
        scrollView.addSubview(stackView)
        addSubview(pageControlView)

        updateView(viewModel: viewModel)
    }

    func setupConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        pageControlView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: pageControlView.topAnchor, constant: -15.0),

            pageControlView.leadingAnchor.constraint(equalTo: leadingAnchor),
            pageControlView.trailingAnchor.constraint(equalTo: trailingAnchor),
            pageControlView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),

            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        ])
    }

    private func setupStackViewContentConstraints() {
        var constraints: [NSLayoutConstraint] = []
        for pageView in stackView.arrangedSubviews {
            pageView.translatesAutoresizingMaskIntoConstraints = false
            constraints.append(pageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor))
        }
        NSLayoutConstraint.activate(constraints)
    }
    
    func updateView() {
    }
    
    private func setupPageControlView(viewModel: CarouselViewModel) {
        pageControlView.numberOfPages = viewModel.pages.count
        pageControlView.currentPage = 0
        pageControlView.hidesForSinglePage = true
        pageControlView.pageIndicatorTintColor = .lightGray
        pageControlView.currentPageIndicatorTintColor = .black
        pageControlView.addTarget(self, action: #selector(pageControlTapped(_:)), for: .valueChanged)
    }
}
// MARK: - Helper for `UIScrollViewDelegate`
extension CarouselView: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControlView.currentPage = currentPageIndex
    }
}
