//
//  CarouselViewModel.swift
//  NYCSchools
//

import UIKit

/// A view model represnting `CarouselView`
struct CarouselViewModel {
    /// An array of `CarouselPageViewModel`
    private (set) var pages: [CarouselPageViewModel]
    
    /// Initializer
    /// - Parameter pages: <#pages description#>
    init(with pages: [CarouselPageViewModel] = []) {
        self.pages = pages
    }
    
    /// Create view assocaited with this view model
    /// - Returns: A view representing CarouselView
    func createView() -> UIView {
        let view = CarouselView()
        view.viewModel = self
        return view
    }
}
