//
//  HSDetailView.swift
//  NYCSchools
//

import UIKit

/// Highschool detail view - Use this view to display high school details
/// This view encapsulates a `CarouselView` to navigate through hs detail pages
class HSDetailView: UIView, ViewProtocol {
    /// Carousel view
    private lazy var carouselView: UIView = {
        let cv = CarouselView()
        let vm: CarouselViewModel
        
        if let viewmodel = viewModel {
            vm = CarouselViewModel(with: CarouselPageViewModelFactory.createPageViewModels(hsDataModel: viewmodel, satDataModel: satDataModel))
        } else {
            vm = CarouselViewModel(with: [])
        }
        cv.viewModel = vm
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        return cv
    }()
    
    /// HS data model
    var viewModel: HSDataModel?
    /// SAT data model
    var satDataModel: SATDataModel?
    
    /// Initializer
    /// - Parameters:
    ///   - viewModel: HS data model
    ///   - satDataModel: SAT data model
    init(viewModel: HSDataModel?, satDataModel: SATDataModel?) {
        self.viewModel = viewModel
        self.satDataModel = satDataModel
        super.init(frame: CGRect.zero)
        setupView()
        setupConstraints()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupConstraints()
    }
    
    func setupView() {
        accessibilityIdentifier = "HSDetailView"
        addSubview(carouselView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                carouselView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
                carouselView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
                carouselView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
                carouselView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            ])
    }
}
