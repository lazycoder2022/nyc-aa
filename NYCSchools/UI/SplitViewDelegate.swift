//
//  SplitViewDelegate.swift
//  NYCSchools
//

import UIKit

/// A class to handle `UISplitViewControllerDelegate` customizations
class SplitViewDelegate: NSObject, UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController,
                             willShow vc: UIViewController,
                             invalidating barButtonItem: UIBarButtonItem)
    {
        if let detailVC = svc.viewControllers.first as? UINavigationController {
            svc.navigationItem.backBarButtonItem = nil
            detailVC.topViewController?.navigationItem.leftBarButtonItem = nil
        }
    }
    
    func splitViewController(_ splitViewController: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool
    {
        guard let navController = primaryViewController as? UINavigationController,
            let controller = navController.topViewController as? HSListViewController
        else {
            return true
        }
        return controller.collapseDetailViewController
    }
}
