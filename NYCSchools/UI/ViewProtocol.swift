//
//  ViewProtocol.swift
//  NYCSchools
//

import UIKit

/// A protocol to define common view related functions
protocol ViewProtocol {    
    func setupView()
    func setupConstraints()
}
