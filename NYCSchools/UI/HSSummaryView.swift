//
//  HSSummaryView.swift
//  NYCSchools
//

import UIKit

/// Summary view containing school information
class HSSummaryView: UIView, ViewProtocol {
    var viewModel: HSDataModel? {
        didSet {
            configureView()
        }
    }
    private lazy var roundedCardView: UIView = {
        let cardView = CardView()
        cardView.translatesAutoresizingMaskIntoConstraints = false
        return cardView
    }()
    
    /// Display title for the data set
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "titleLabel-HSSummaryView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0 
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        return  label
    }()
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "addressLabel-HSSummaryView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    private lazy var contactLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "phoneLabel-HSSummaryView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    init(viewModel: HSDataModel?) {
        self.viewModel = viewModel
        super.init(frame: CGRect.zero)
        setupView()
        setupConstraints()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupConstraints()
    }
    
    // MARK: - ViewProtocol
    func setupView() {
        accessibilityIdentifier = "HSSummaryView"
        addSubview(roundedCardView)
        addSubview(titleLabel)
        addSubview(addressLabel)
        addSubview(contactLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                roundedCardView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
                roundedCardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
                roundedCardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
                titleLabel.topAnchor.constraint(equalTo: roundedCardView.topAnchor, constant: 16),
                titleLabel.leadingAnchor.constraint(equalTo: roundedCardView.leadingAnchor, constant: 16),
                titleLabel.trailingAnchor.constraint(equalTo: roundedCardView.trailingAnchor, constant: -16),
                titleLabel.bottomAnchor.constraint(equalTo: addressLabel.topAnchor, constant: -8),
                
                addressLabel.leadingAnchor.constraint(equalTo: roundedCardView.leadingAnchor, constant: 16),
                addressLabel.trailingAnchor.constraint(equalTo: roundedCardView.trailingAnchor, constant: -8),
                addressLabel.bottomAnchor.constraint(equalTo: contactLabel.topAnchor, constant: -8),

                contactLabel.leadingAnchor.constraint(equalTo: roundedCardView.leadingAnchor, constant: 16),
                contactLabel.trailingAnchor.constraint(equalTo: roundedCardView.trailingAnchor, constant: -8),
                roundedCardView.bottomAnchor.constraint(equalTo: contactLabel.bottomAnchor, constant: 16),
                
                heightAnchor.constraint(equalTo: roundedCardView.heightAnchor, constant: 16)
            ])
    }
    
    func updateView() {
        configureView()
    }
    
    // MARK: - Helpers
    
    /// Configure view based on datamodels
    func configureView() {
        if let viewModel = viewModel {
            titleLabel.text = viewModel.schoolName
            addressLabel.text = viewModel.address
            contactLabel.text = viewModel.contact
        } else {
            titleLabel.text = "Please select a school"
        }
    }
}
