//
//  ViewController.swift
//  NYCSchools
//

import UIKit

/// A list view (table view) showing list of schools
/// Supports pull to refresh and search functionality
final class HSListViewController: UIViewController {
    /// Cell identifier
    private let hsListViewCellIdentifier = "HSListViewCellIdentifier"
    /// Tableview for showing the list of schools
    private let tableView = UITableView(frame: .zero, style: .plain)
    /// Refresh control associated with the table view
    private let refreshControl = UIRefreshControl()
    /// Search controller for seaching the content
    private let searchController = UISearchController(searchResultsController: nil)
    /// An instance of HSListViewModel that drives the list screen
    private var viewModel = HSListViewModel(service: NYCHSDataApiService(session: .shared),
                                            satService: NYCSATDataApiService(session: .shared))
    /// Specify if detail view should be collapsed. This is mainly used for handling split view controller
    var collapseDetailViewController = true
    /// Stackview that contains tableview
    private lazy var stackView: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .vertical
        return sv
    }()
    /// Loading indicator that is used while data is being loaded
    private lazy var loadingIndicator: UIView = {
        let loadingIndicatorView = UIView(frame: view.frame)
        loadingIndicatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        loadingIndicatorView.isHidden = true
        view.insertSubview(loadingIndicatorView, aboveSubview: tableView)
        // Activity indicator
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityIndicator.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
        activityIndicator.color = .blue
        activityIndicator.sizeToFit()
        activityIndicator.center = CGPoint(x: loadingIndicatorView.bounds.midX, y: loadingIndicatorView.bounds.midY)
        activityIndicator.startAnimating()
        
        loadingIndicatorView.addSubview(activityIndicator)
        return loadingIndicatorView
    }()
    
    /// Convinience initializer
    /// - Parameter viewmodel: <#viewmodel description#>
    convenience init(viewmodel: HSListViewModel) {
        self.init(nibName: nil, bundle: nil)
        self.viewModel = viewmodel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Additional setup
        setupUI()
        setupObservers()
        showSpinner()
        fetchHSData()
    }
    
    deinit {
        viewModel.cancel()
    }
    
    // MARK: - View setup
    func setupUI() {
        // TODO: Ignoring localization for now
        navigationItem.title = "NYC Schools"
        let add = UIBarButtonItem(
            title: NSLocalizedString("Map", comment: "Map"),
            style: .plain,
            target: self,
            action: #selector(self.showMap)
        )
        navigationItem.rightBarButtonItem = add
        setupTableView()
        setupStackView()
        setupSearchController()
        setupConstraints()
    }
    
    func setupTableView() {
        view.backgroundColor = .white
        tableView.accessibilityIdentifier = "tableView-HSListViewController"
        tableView.separatorStyle   = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        tableView.register(HSListViewCell.self, forCellReuseIdentifier: hsListViewCellIdentifier)
        refreshControl.addTarget(self, action: #selector(refreshHSData(_:)), for: .valueChanged)
    }
    
    func setupStackView() {
        stackView.addArrangedSubview(tableView)
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
    }
    
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        // TODO: Ignoring localization for now
        searchController.searchBar.placeholder = "Search schools"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
    }
    
    @objc private func showMap() {
        let mapVC = HSMapViewController(dataModels: viewModel.hsDataModels ?? [])
        showDetailViewController(mapVC, sender: self)
    }
    
}

// MARK: - Helper functions
extension HSListViewController {
    /// Fetch school data using api
    /// This function delegates responsibility to download data to view model
    func fetchHSData() {
        viewModel.fetchHSData()
        viewModel.fetchSATData()
    }
    
    /// Reload table view data
    func reload() {
        tableView.reloadData()
    }
    
    /// Setup observers when data updates
    func setupObservers() {
        // Use this observer when data fetch succeeds
        viewModel.refreshObserver = { [weak self] in
            self?.reload()
            self?.refreshControl.endRefreshing()
            self?.hideSpinner()
        }
        // Use this observer when data fetch fails
        viewModel.errorObserver = { [weak self] (error) -> Void in
            self?.refreshControl.endRefreshing()
            self?.hideSpinner()
            self?.showError(error: error)
        }
    }
    
    /// Refresh data by calling remote api. This function is used by refresh control
    /// - Parameter sender: Sender invoking the refresh
    @objc private func refreshHSData(_ sender: Any) {
        fetchHSData()
    }
    
    /// Display an error alert. Uses localizedDescription from the error to display the message.
    /// TODO:  Handle error message better i.e translate to a different error message if required.
    /// - Parameter error: Error
    func showError(error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UITableView helpers
extension HSListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count(isSearching: isSearching)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let hsListViewCell = tableView.dequeueReusableCell(withIdentifier: hsListViewCellIdentifier) as? HSListViewCell else {
            return UITableViewCell()
        }
        let ret = viewModel.model(for: indexPath.row, isSearching: isSearching)
        guard let hsDataModel = ret.0 else {
            // TODO: This is not the best way to handle missing rows.
            return UITableViewCell()
        }
        hsListViewCell.configure(with: hsDataModel)
        return hsListViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 160
        tableView.rowHeight = UITableView.automaticDimension
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        collapseDetailViewController = false
        let ret = viewModel.model(for: indexPath.row, isSearching: isSearching)
        if let hsDataModel = ret.0 {
            let detailVC = HSDetailViewController(model: hsDataModel, satDataModel: ret.1)
            showDetailViewController(detailVC, sender: self)
        }
    }
}

// MARK: - Search helpers
extension HSListViewController: UISearchResultsUpdating {
    /// Returns true if seachBar has text and searchController is in focus
    var isSearching: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    /// Determine if searchBar is empty or not
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    /// Update the table view with results from search
    /// - Parameter searchController: SearchController
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        viewModel.filterContent(for: searchBar.text!)
    }
}

// MARK: - Spinners
extension HSListViewController {
    /// Show spinner
    private func showSpinner() {
        loadingIndicator.isHidden = false
    }
    /// Hide spinner
    private func hideSpinner() {
        loadingIndicator.isHidden = true
    }
}
