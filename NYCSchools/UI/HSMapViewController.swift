//
//  HSMapViewController.swift
//  NYCSchools
//

import UIKit
import MapKit

/// Map view showing all the schools
/// TODO: Build feature rich call outs and actions
class HSMapViewController: UIViewController {
    private var dataModels: [HSDataModel] = []
    
    private lazy var schoolsMapView: MKMapView = {
        let mv = MKMapView()
        mv.translatesAutoresizingMaskIntoConstraints = false
        return mv
    }()
    
    convenience init(dataModels: [HSDataModel]) {
        self.init(nibName: nil, bundle: nil)
        self.dataModels = dataModels
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    func setupView() {
        view.backgroundColor = .white
        view.addSubview(schoolsMapView)
        var annotations = [MKAnnotation]()
        for schoolModel in dataModels {
            if let latlong = schoolModel.latlong {
                let annotation = MKPointAnnotation()
                annotation.coordinate = latlong
                annotation.title = schoolModel.schoolName
                annotations.append(annotation)
            }
        }
        schoolsMapView.showAnnotations(annotations, animated: true)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                schoolsMapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                schoolsMapView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                schoolsMapView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                schoolsMapView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
    }
}
