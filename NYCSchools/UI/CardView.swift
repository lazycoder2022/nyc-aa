//
//  CardView.swift
//  NYCSchools
//

import UIKit

/// A rounded UIView that has light borders
/// TODO: Make this class configurable to accept border and shadow parameters
class CardView: UIView, ViewProtocol {
    /// Internal containerview that will hold the contents
    private lazy var containerView: UIView = {
        let cardView = UIView()
        cardView.translatesAutoresizingMaskIntoConstraints = false
        cardView.backgroundColor = .white
        return cardView
    }()
    
    /// Default initializer.
    /// - Parameter frame: View frame
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.layer.bounds, cornerRadius: containerView.layer.cornerRadius).cgPath
        containerView.layer.borderWidth = 0.0
    }
    
    // MARK: - ViewProtocol
    func setupView() {
        setupBorders()
        addSubview(containerView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                containerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
                containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
                containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
                containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
            ])
    }
    
    func setupBorders() {
        layer.cornerRadius = 20
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0.2
    }
    
    func setupShadows() {
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 4)
        containerView.layer.shadowOpacity = 0.10
        containerView.layer.shadowRadius = 8.0
        containerView.layer.masksToBounds = false
        containerView.layer.shouldRasterize = true
        containerView.clipsToBounds = false
        containerView.layer.rasterizationScale = UIScreen.main.scale
        containerView.layer.cornerRadius = 8
    }
}
