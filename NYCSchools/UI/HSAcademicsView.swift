//
//  HSSATView.swift
//  NYCSchools
//

import UIKit

/// Academics view for displaying academic and SAT information
class HSAcademicsView: UIView {
    /// SAT data model
    var satDataModel: SATDataModel? {
        didSet {
            configureView()
        }
    }
    
    /// High school data model containing school data
    var hsDataModel: HSDataModel? {
        didSet {
            configureView()
        }
    }
    
    /// Rounded card view
    private lazy var roundedCardView: UIView = {
        let cardView = CardView()
        cardView.translatesAutoresizingMaskIntoConstraints = false
        cardView.backgroundColor = .white
        return cardView
    }()
    
    /// Display title for the data set
    private lazy var attendanceRateLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "attendanceRateLabel-HSAcademicsView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        //label.font = UIFont.systemFont(ofSize: 18.0, weight: .regular)
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return  label
    }()
    
    private lazy var testTakerLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "testTakerLabel-HSAcademicsView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    private lazy var criticalReadingAvgScoreLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "criticalReadingAvgScoreLabel-HSAcademicsView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    private lazy var mathAvgScoreLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "mathAvgScoreLabel-HSAcademicsView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    private lazy var writingAvgScoreLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "writingAvgScoreLabel-HSAcademicsView"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        label.textColor = UIColor.lightGray
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    private lazy var satContentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.accessibilityIdentifier = "contentStackView-HSAcademicsView"
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.backgroundColor = UIColor(rgbValue: 0xf6f7f9)
        return stackView
    }()
    
    private lazy var horizontalStackView1: UIStackView = {
        let stackView = UIStackView()
        stackView.accessibilityIdentifier = "horizontalStackView1-HSAcademicsView"
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = 12
        return stackView
    }()

    private lazy var horizontalStackView2: UIStackView = {
        let stackView = UIStackView()
        stackView.accessibilityIdentifier = "horizontalStackView2-HSAcademicsView"
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = 12
        return stackView
    }()
    
    init(hsDataModel: HSDataModel?, satModel: SATDataModel?) {
        self.satDataModel = satModel
        self.hsDataModel = hsDataModel
        super.init(frame: CGRect.zero)
        setupView()
        setupConstraints()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupConstraints()
    }
 
    func setupView() {
        accessibilityIdentifier = "HSAcademicView"
        addSubview(roundedCardView)
        addSubview(satContentStackView)
        
        horizontalStackView1.addArrangedSubview(testTakerLabel)
        horizontalStackView2.addArrangedSubview(criticalReadingAvgScoreLabel)
        horizontalStackView2.addArrangedSubview(writingAvgScoreLabel)
        horizontalStackView2.addArrangedSubview(mathAvgScoreLabel)
        satContentStackView.addArrangedSubview(horizontalStackView1)
        satContentStackView.addArrangedSubview(horizontalStackView2)
        
        roundedCardView.addSubview(attendanceRateLabel)
        configureView()
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                roundedCardView.topAnchor.constraint(equalTo: topAnchor),
                roundedCardView.leadingAnchor.constraint(equalTo: leadingAnchor),
                roundedCardView.trailingAnchor.constraint(equalTo: trailingAnchor),
                roundedCardView.bottomAnchor.constraint(equalTo: bottomAnchor),
                
                attendanceRateLabel.topAnchor.constraint(equalTo: roundedCardView.topAnchor, constant: 16),
                attendanceRateLabel.leadingAnchor.constraint(equalTo: roundedCardView.leadingAnchor, constant: 8),
                attendanceRateLabel.trailingAnchor.constraint(equalTo: roundedCardView.trailingAnchor, constant: -4),
                
                satContentStackView.topAnchor.constraint(equalTo: attendanceRateLabel.bottomAnchor, constant: 16),
                satContentStackView.leadingAnchor.constraint(equalTo: roundedCardView.leadingAnchor),
                satContentStackView.trailingAnchor.constraint(equalTo: roundedCardView.trailingAnchor),
                satContentStackView.bottomAnchor.constraint(equalTo: mathAvgScoreLabel.bottomAnchor, constant: 16),
            ])
    }
    
    /// Configure view vased on data from hsDataModel and satDataModel
    func configureView() {
        if let attendanceRate = hsDataModel?.attendanceRatePercentage {
            attendanceRateLabel.setTitle(titleText: "\(attendanceRate)%", subTitleText: "Attendance rate")
        }
        if let numOfSatTestTakers = satDataModel?.numOfSatTestTakers {
            testTakerLabel.setTitle(titleText: numOfSatTestTakers, subTitleText: "SAT attendees")
        } else {
            testTakerLabel.text = "No SAT data available"
        }
        if let satMathAvgScore = satDataModel?.satMathAvgScore {
            mathAvgScoreLabel.setTitle(titleText: satMathAvgScore, subTitleText: "Math avg")
        }
        if let satCriticalReadingAvgScore = satDataModel?.satCriticalReadingAvgScore {
            criticalReadingAvgScoreLabel.setTitle(titleText: satCriticalReadingAvgScore, subTitleText: "Critical reading")
            
        }
        if let satWritingAvgScore = satDataModel?.satWritingAvgScore {
            writingAvgScoreLabel.setTitle(titleText: satWritingAvgScore, subTitleText: "Writing avg")
        }
    }
}
