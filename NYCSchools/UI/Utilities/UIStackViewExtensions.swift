//
//  UIStackViewExtensions.swift
//  NYCSchools
//

import UIKit

extension UIStackView {
    @discardableResult
    /// Remove all arranged subviews
    /// - Returns: Removed UIViews
    func removeAllArrangedSubviews() -> [UIView] {
        return arrangedSubviews.reduce([UIView]()) {
            $0 + [removeArrangedSubViewProperly($1)]
        }
    }
    
    /// Removes arragned subviews, deactivates constraints, removes view from the superview.
    /// - Parameter view: view to be removed
    /// - Returns: view that was removed
    func removeArrangedSubViewProperly(_ view: UIView) -> UIView {
        removeArrangedSubview(view)
        NSLayoutConstraint.deactivate(view.constraints)
        view.removeFromSuperview()
        return view
    }
}
