//
//  UIColorExtensions.swift
//  NYCSchools
//

import UIKit

extension UIColor {
    /// Creates a UIColor based on rgb value
    /// EX: UIColor(rgbValue: 0xf6f7f9)
    /// - Parameter rgbValue: in specifying RGB value
    public convenience init(rgbValue: Int) {
        self.init(
            red: CGFloat(((rgbValue & 0xFF0000) >> 16)) / 255.0,
            green: CGFloat(((rgbValue & 0xFF00) >> 8)) / 255.0,
            blue: CGFloat((rgbValue & 0xFF)) / 255.0,
            alpha: 1.0
        )
    }
}
