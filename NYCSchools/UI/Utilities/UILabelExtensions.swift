//
//  UILabelExtensions.swift
//  NYCSchools
//

import UIKit

extension UILabel {
    /// Setup label as title/subtitle
    /// Title is shown with bold font, subtitle is shown with light font
    /// - Parameters:
    ///   - titleText: Title text
    ///   - subTitleText: Subtitle text
    public func setTitle(titleText: String, subTitleText: String) {
        let attributedString = attributedString(for: titleText, and: subTitleText)
        numberOfLines = 0
        textAlignment = .center
        attributedText = attributedString
        lineBreakMode = .byWordWrapping
        sizeToFit()
    }
    
    /// Returns attributed string for title and subtitle. Title s set at font size of 20. Subtitle is set at font size of 12.
    /// The text is center aligned.
    /// - Parameters:
    ///   - title: Title
    ///   - subTitle: Subtitle
    /// - Returns: Returns attributed string for title and subtitle
    fileprivate func attributedString(for title: String, and subTitle: String) -> NSAttributedString {
        // title
        let titleWithNewline = title + "\r\n"
        let titleAttributedText = NSMutableAttributedString(string: titleWithNewline, attributes: [.font: UIFont.systemFont(ofSize: 20.0, weight: .bold)])
        let titleStyle = NSMutableParagraphStyle()
        titleStyle.alignment = .center
        titleStyle.minimumLineHeight = 16
        titleStyle.maximumLineHeight = 24
        titleAttributedText.addAttribute(.paragraphStyle, value: titleStyle, range: NSRange(location: 0, length: titleWithNewline.count))
        titleAttributedText.addAttribute(.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: title.count))
        
        // subtitle
        let subTitleAttributedText = NSMutableAttributedString(string: subTitle, attributes: [.font: UIFont.systemFont(ofSize: 12.0, weight: .light)])
        let subTitleStyle = NSMutableParagraphStyle()
        subTitleStyle.alignment = .center
        subTitleStyle.lineHeightMultiple = 1.3
        subTitleAttributedText.addAttribute(.paragraphStyle, value: subTitleStyle, range: NSRange(location: 0, length: subTitle.count))
        subTitleAttributedText.addAttribute(.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: subTitle.count))
        
        titleAttributedText.append(subTitleAttributedText)
        return titleAttributedText
    }
}

