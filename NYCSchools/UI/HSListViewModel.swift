//
//  HSListViewModel.swift
//  NYCSchools
//

import Foundation
import UIKit

/// Refresh observer type
typealias HSDataRefreshObserver = () -> Void
/// Error observer type
typealias ErrorObserver = (Error) -> Void

/// View model for handling list screen that displays list of schools
final class HSListViewModel {
    /// High school data fetching task
    private var hsDataFetchingTask: Task<Void, Error>?
    /// SAT data fetching task
    private var satDataFetchingTask: Task<Void, Error>?
    /// HS Data API service instance
    private let hsDataApiService: NYCHSDataApiService
    /// SAT Data API service instance
    private let satDataApiService: NYCSATDataApiService
    /// Data model for HS data
    var hsDataModels: [HSDataModel]? {
        didSet {
            DispatchQueue.main.async {
                self.refreshObserver?()
            }
        }
    }
    /// Filtered list of schools
    private var filteredHSDataModels: [HSDataModel]?
    /// SAT data model
    private var satDataModels: [SATDataModel]? {
        didSet {
            DispatchQueue.main.async {
                self.refreshObserver?()
            }
        }
    }
    /// Refresh observer
    var refreshObserver: HSDataRefreshObserver?
    /// Error observer
    var errorObserver: ErrorObserver?

    /// Initialize an instance of `HSListViewModel`
    /// - Parameters:
    ///   - service: HS Data service api instance
    ///   - satService: SAT Data service api instance
    init(service: NYCHSDataApiService, satService: NYCSATDataApiService) {
        self.hsDataApiService = service
        self.satDataApiService = satService
    }
    
    /// Fetch HS Data. This function asynchronously downloads the data using service api.
    /// If api is successful, the internal data model is updated
    /// If api returns a failure, the function calls `errorObserver` on main queue
    func fetchHSData() {
        hsDataFetchingTask = Task {
            do {
                hsDataModels = try await hsDataApiService.fetchNYCHSData()
            } catch {
                DispatchQueue.main.async {
                    self.errorObserver?(error)
                }
            }
        }
    }
    
    /// Fetch SAT data. This function asynchronously downloads the data using service api.
    /// If api is successful, the internal data model is updated
    /// If api returns a failure, the function calls `errorObserver` on main queue
    func fetchSATData() {
        satDataFetchingTask = Task {
            do {
                satDataModels = try await satDataApiService.fetchNYCSATData()
            }  catch {
                DispatchQueue.main.async {
                    self.errorObserver?(error)
                }
            }
        }
    }
    
    /// Cancel tasks
    func cancel() {
        hsDataFetchingTask?.cancel()
        satDataFetchingTask?.cancel()
    }
    
    /// Returns count of schools to be displayed in the list. The results are filtered results if isSearching is set to true
    /// - Parameter isSearching: true if search is active
    /// - Returns: Number of schools
    func count(isSearching: Bool) -> Int {
        if isSearching {
            return filteredHSDataModels?.count ?? 00
        }
        return hsDataModels?.count ?? 0
    }
    
    /// Filter schools based on the search criterion.
    /// This function simply performs contains search for the seach term.
    /// TODO: Improve search to include other filter criteria (EG SAT score, academic program etc)
    /// Once filtered, the table view is refreshed.
    /// - Parameter searchText: Search text to filter by
    func filterContent(for searchText: String) {
        filteredHSDataModels = hsDataModels?.filter { hsData -> Bool in
            return hsData.schoolName.lowercased().contains(searchText.lowercased())
        }
        refreshObserver?()
    }
    
    /// Returns model associated with the row. The results are dependent on if seach is active or not.
    /// If Search is active (i.e. isSearching is set to true), the returned result is returned from the filtered list.
    /// - Parameters:
    ///   - row: Row
    ///   - isSearching: True is search is active
    /// - Returns: Model associated with the supplied row.
    func model(for row: Int, isSearching: Bool) -> (HSDataModel?, SATDataModel?) {
        if isSearching {
            let filteredHSDataModel = filteredHSDataModels?[row]
            let satDataModel = satDataModels?.first { $0.dbn == filteredHSDataModel?.dbn }
            return(filteredHSDataModel, satDataModel)
        }
        let hsDataModel = hsDataModels?[row]
        let satDataModel = satDataModels?.first { $0.dbn == hsDataModel?.dbn }
        return (hsDataModel, satDataModel)
    }
}
