//
//  HSDetailViewController.swift
//  NYCSchools
//

import Foundation
import UIKit

/// A detail view controller to display selected high school details.
/// This class uses a stackview to encapsulate `HSSummaryView` and `HSDetailView`
class HSDetailViewController: UIViewController {
    /// Data model for select high school
    private var model: HSDataModel?
    /// A data model containing SAT related details
    private var satDataModel: SATDataModel?
    
    /// Stackview containing
    private lazy var stackView: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .vertical
        return sv
    }()
    
    /// Initialize view controller with given `HSDataModel` and `SATDataModel`
    /// - Parameters:
    ///   - model: Data model containing HS information
    ///   - satDataModel: SAT data model for the selected HS. 
    init(model: HSDataModel? = nil, satDataModel: SATDataModel? = nil) {
        self.model = model
        self.satDataModel = satDataModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setupStackView()
        setupConstraints()
    }
    
    func setupStackView() {
        let summaryView = HSSummaryView(viewModel: model)
        let detailView = HSDetailView(viewModel: model, satDataModel: satDataModel)
        stackView.addArrangedSubview(summaryView)
        stackView.addArrangedSubview(detailView)
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        
        summaryView.configureView()
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
    }
}
