//
//  HSDataModel.swift
//  NYCSchools
//

import Foundation
import MapKit

/// Data model for High School data as obtained from the api/service
struct HSDataModel: Codable {
    let dbn, schoolName: String
    let boro, overviewParagraph: String?
    let school10ThSeats, academicopportunities1, academicopportunities2, ellPrograms: String?
    let neighborhood, buildingCode, location, phoneNumber: String?
    let faxNumber, schoolEmail, website, subway: String?
    let bus, grades2018, finalgrades, totalStudents: String?
    let extracurricularActivities, schoolSports, attendanceRate, pctStuEnoughVariety: String?
    let latitude, longitude, borough: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overviewParagraph = "overview_paragraph"
        case school10ThSeats = "school_10th_seats"
        case academicopportunities1, academicopportunities2
        case ellPrograms = "ell_programs"
        case neighborhood
        case buildingCode = "building_code"
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website, subway, bus, grades2018, finalgrades
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case pctStuEnoughVariety = "pct_stu_enough_variety"
        case latitude, longitude, borough
    }
    
    /// A number formatter for displaying data with some reasonable assumptions
    private static var numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        return formatter
    }()
}

// MARK: - Helpers
extension HSDataModel {
    
    /// Returns an address string (if available)
    var address: String? {
        guard let index = location?.firstIndex(of: "("),
              let addressStr = location?[..<index] else {
            return location
        }
        return String(addressStr)
    }
    
    /// Returns a contact string of format Phone / Email
    var contact: String? {
        if let phone = phoneNumber {
            if let email = schoolEmail {
                return phone + " / " + email
            }
            return phone
        } else if let email = schoolEmail {
            return email
        }
        return nil
    }
    
    /// Returns a string that lists academic oppprtunities
    var academicOpportunities: String? {
        if let academicopportunities1 = academicopportunities1 {
            return academicopportunities1 + " " + (academicopportunities2 ?? "")
        }
        return academicopportunities2
    }
    
    /// Returns a string containing commute information. Includes Borough, Neighborhood, bus and subway information
    var gettingThere: String? {
        var commuteData = ""
        if let borough = borough, let neighborhood = neighborhood {
            commuteData = "Borough: \(borough), Neighborhood: \(neighborhood)\n"
        }
        if let bus = bus {
            commuteData += "Bus: \(bus)\n"
        }
        if let subway = subway {
            commuteData += "Subway: \(subway)"
        }
        return commuteData.isEmpty ? nil : commuteData
    }
    
    /// Returns Latitude and longitude of the school
    var latlong: CLLocationCoordinate2D? {
        if let latitude = latitude, let longitude = longitude,
           let latDeg = CLLocationDegrees(latitude), let longDeg = CLLocationDegrees(longitude) {
            return CLLocationCoordinate2D(latitude: latDeg, longitude: longDeg)
        }
        return nil
    }
    
    /// Returns extra curricular activities support by the school
    var extraActivities: String? {
        var activities: String = ""
        if let extracurricularActivities = extracurricularActivities {
            activities =  "Extracurricular activities: \(extracurricularActivities)\n"
        }
        if let schoolSports = schoolSports {
             activities += "School sports: \(schoolSports)\n"
        }
        if let ellPrograms = ellPrograms {
            activities += "ELL programs: \(ellPrograms)"
        }
        return activities
    }
    
    /// Returns attendance rate percentage
    var attendanceRatePercentage: String? {
        if let attendanceRate = attendanceRate, let num = Double(attendanceRate) {
            return HSDataModel.numberFormatter.string(from: NSNumber(value: num * 100.0))
        }
        return nil
    }
}

