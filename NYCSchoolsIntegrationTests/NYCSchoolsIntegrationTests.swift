//
//  NYCSchoolsIntegrationTests.swift
//  NYCSchoolsIntegrationTests
//

import XCTest
@testable import NYCSchools

/// Test cases for testing apis
class NYCSchoolsIntegrationTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNYCDataServiceAPI() throws {
        let expect = XCTestExpectation(description: "testNYCDataServiceAPI ")
        let service = NYCHSDataApiService(session: .shared)
        Task {
            do {
                let hsDataModels = try await service.fetchNYCHSData()
                XCTAssertEqual(hsDataModels.count, 440)
                expect.fulfill()
            } catch {
                XCTFail(error.localizedDescription)
                expect.fulfill()
            }
        }
        wait(for: [expect], timeout: 10.0)
    }
    
    func testNYCSATServiceAPI() throws {
        let expect = XCTestExpectation(description: "testNYCSATServiceAPI ")
        let service = NYCSATDataApiService(session: .shared)
        Task {
            do {
                let satDataModels = try await service.fetchNYCSATData()
                XCTAssertEqual(satDataModels.count, 478)
                expect.fulfill()
            } catch {
                XCTFail(error.localizedDescription)
                expect.fulfill()
            }
        }
        wait(for: [expect], timeout: 10.0)
    }
}
