# ``NYCSchools``
NYCSchools is a sample app that allows you to browse NYC area schools.

## Overview
NYCSchools uses data based on public apis published by the city of New York. 
See [NYC School directory](https://data.cityofnewyork.us/Education/2017-DOE-High-School-Directory/s3k6-pzi2)
and the [SAT data](https://data.cityofnewyork.us/Education/2012-SAT-Results/f9bf-2cp4)

**App features**
Architecture:
  -- Using MVVM, factory patterns
  -- Structured concurrency model (async/await) for n/w stack
  -- Separation of concerns between UI and business logic
  -- Several custom view components that can be replaced or customized for a specific design system
- Fetch school and sat data by using published api
- Smooth scrolling through the list 
- Narrow search by entering a search term (search by name)
- Select a school to view details 
  - Overview - Get basic information about the school. 
  - Getting there - Get information about public transportation (Bus, Subway) and other information about neighbourhood. Display interactive map of the location.
  - Academics - Various programs offered, attendance rate and SAT data (if available)
- Supports iPad and iPhone (landscape/portrait orientatons)
- Display Map of all schools to look at all schools together
- Testing examples for Unit, UI, Snapshot and Integration tests
- Documentation using DocC

**Future enhacements** 

_UI_
- Improve iPad designs to display more data
- Support dark mode
- Accessibility support for all screens
- Improve overall UI, color schemes etc
- Add sound and animations to make app more delightful
- Sort functionality for list screen
- Pagination for list
- Search and compare on Map
- Additional search filters (for example search by SAT score etc.)

_Others_
- Support for localization 
- Local data storage (caching)
- User analytics 
- Improve error handling, log errors to a remote server
- Handle network errors better
- Break down data models into logical groupings
- Save favorites
- Compare schools
- Enhace test suite to include mock tests for n/w and UI tests, complete other tests for view models etc.
- Improve documentation 
